const express = require('express');
const bodyParser = require('body-parser'); // middleware to parse form data
const app = express();
const port = 3000;

// Use body-parser middleware
app.use(bodyParser.urlencoded({ extended: true }));

// Set up a simple homepage route
app.get('/', (req, res) => {
  res.send(`
    <h1>Hello, welcome to the homepage!</h1>
    <form method="post" action="/submit">
      <label for="name">Name:</label>
      <input type="text" id="name" name="name" required>
      <br>
      <label for="email">Email:</label>
      <input type="email" id="email" name="email" required>
      <br>
      <button type="submit">Submit</button>
    </form>
  `);
});

// Handle form submission
app.post('/submit', (req, res) => {
  const { name, email } = req.body;
  res.send(`<h2>Form submitted successfully!</h2><p>Name: ${name}</p><p>Email: ${email}</p>`);
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
